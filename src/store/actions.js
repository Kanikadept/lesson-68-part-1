import axios from "axios";


export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';


export const increment = () => ({type: INCREMENT});
export const decrement = () => ({type: DECREMENT});
export const add = value => ({type: ADD, value});
export const subtract = value => ({type: SUBTRACT, value});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});

export const fetchCounter = () => {
    return async dispatch => {
        dispatch(fetchCounterRequest());

        try {
            const url = 'https://lesson-68-redux-thunk-default-rtdb.firebaseio.com/counter.json'
            const response = await axios.get(url);
            dispatch(fetchCounterSuccess(response.data));
        } catch (e) {
            dispatch(fetchCounterFailure());
        }
    };
};

export const incrementCounter = () => {

    return async (dispatch, getState) => {
        dispatch(increment());
        const updatedCounter = getState().counter;

        try {
            const url = 'https://lesson-68-redux-thunk-default-rtdb.firebaseio.com/counter.json'
            const response = await axios.put(url, updatedCounter);
            dispatch(fetchCounterSuccess(response.data));
        } catch (e) {
            dispatch(fetchCounterFailure());
        }
    }
}

export const decrementCounter = () => {

    return async (dispatch, getState) => {
        dispatch(decrement());
        const updatedCounter = getState().counter;

        try {
            const url = 'https://lesson-68-redux-thunk-default-rtdb.firebaseio.com/counter.json'
            const response = await axios.put(url, updatedCounter);
            dispatch(fetchCounterSuccess(response.data));
        } catch (e) {
            dispatch(fetchCounterFailure());
        }
    }
}

export const addCounter = (value) => {

    return async (dispatch, getState) => {
        dispatch(add(value));
        const updatedCounter = getState().counter;

        try {
            const url = 'https://lesson-68-redux-thunk-default-rtdb.firebaseio.com/counter.json'
            const response = await axios.put(url, updatedCounter);
            dispatch(fetchCounterSuccess(response.data));
        } catch (e) {
            dispatch(fetchCounterFailure());
        }
    }
}

export const subtractCounter = (value) => {

    return async (dispatch, getState) => {
        dispatch(subtract(value));
        const updatedCounter = getState().counter;

        try {
            const url = 'https://lesson-68-redux-thunk-default-rtdb.firebaseio.com/counter.json'
            const response = await axios.put(url, updatedCounter);
            dispatch(fetchCounterSuccess(response.data));
        } catch (e) {
            dispatch(fetchCounterFailure());
        }
    }
}
