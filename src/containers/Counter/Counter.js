import React, {useEffect} from 'react';
import './Counter.css';
import {useDispatch, useSelector} from "react-redux";
import {
    addCounter,
    decrementCounter,
    fetchCounter,
    incrementCounter, subtractCounter
} from "../../store/actions";

const STEP_VALUE = 5;

const Counter = () => {
  const dispatch = useDispatch();
  const state = useSelector(state => state);

    useEffect(() => {
        dispatch(fetchCounter());
    }, [dispatch]);

  const buttons = {
      'Increase': () => dispatch(incrementCounter()),
      'Decrease': () => dispatch(decrementCounter()),
      ['Increase by ' + STEP_VALUE]: () => dispatch(addCounter(STEP_VALUE)),
      ['Decrease by ' + STEP_VALUE]: () => dispatch(subtractCounter(STEP_VALUE)),
  }

  return (
    <div className="Counter">
      <p>Counter:</p>
      <h1>{state.counter}</h1>
        {Object.keys(buttons).map(title => (
            <button key={title} onClick={buttons[title]}>{title}</button>
        ))}
    </div>
  );
};

export default Counter;